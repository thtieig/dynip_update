# README #

Here a script that I have created to update your Dynamic DNS service.
You can run it manually or put it within a cron to run every few minutes.
It sends the update ONLY if the IP has changed. So you will avoid any "abuse" error, in case of too many attempts to update the IP.

This script currently works with Internet.bs, NO-IP.com and Cloudflare services.
The sample conf file uses Cloudflare as default provider.

It requires `curl` package.
Tested on Raspberry Pi raspbian, Ubuntu 18.04 and Debian 9 distros.

This script is composed by 3 files:
* Config file: `dynip_update.conf.example`
* Script file: `dynip_update`
* Cron file: `cron_dynip_update`


### What is this repository for? ###

* Update your dynamic DNS provider (InternetBS / NO-IP.com / Cloudflare)
* Version: 3.2.2

### How do I get set it up? ###

1. Automatic installation/Uninstall:
Simply run `sudo install.sh` to install. The install script will backup any previous config files.
To uninstall: `sudo uninstall.sh`. The uninstall will remove ANY file (current and previous) related to this script.

2. Manual Installation:
- `sudo cp dynip_update.conf.example /etc/dynip_update.conf`
- Modify `/etc/dynip_update.conf` accordingly with your parameters. Read carefully the comments.
- `sudo chmod 600 /etc/dynip_update.conf`
- `sudo cp dynip_update /usr/local/bin/dynip_update`
- `sudo cp cron_dynip_update /etc/cron.d/dynip_update` 
- Edit `/etc/cron.d/dynip_update` if you'd like to run this script automatically within a cron every 5 minuts. By default is DISABLED.

3. Dependencies: *curl, dig, sendmail*

#### NOTE
Once set it up, you can run the script manually, using the flag `-f`, to force the update.
e.g. `/usr/local/bin/dynip_update -f`


### Who do I talk to? ###

* Repo owner or admin
